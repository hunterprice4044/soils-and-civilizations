ESS 120 Fall 2019
Lecture 5—Possible Exam Questions
1.Regarding the Yellow River and its floodplain, how do rivers get “above” their flood plains?
	the river as it carries the bed load. the bed fills with sediment , and it breaches. the bed eventually rises and natural levees form

2.Explain: “In the last reckoning all things are purchased with food...”
	People will do anything for food. There is a connection between peoples actions and food.

3.Be able to name the soil forming factors
	1.  Parent material: cant change it
	2.  Climate: Cant control the climate
	3.  Biota: Woodchucks, us, earthworms, etc
	4.  Topography: Trees by the water
	5.  Time: How long has the soil been forming. Age of soil.

4.What would a young soil look like?
	soil = dynamic natural body formaed by the combined effects of climate and biota, as moderated by topography, acting on parent materials over time
	- Biosequence
	- Toposequence
	- Chronosequence
	
	soil = f(climate,biota,parent materials, topography, time)

5.What is the significance of the different geologic materials to later soil productivity
	The profile is boring. Entisol and Mollisol. Mollisol is older. Entisol is more sandy. ABC profile. 
Deepening of profile = older soil. 
Red-Bed = coal
It is significant in order to identify productive soils and age soils. Geologists are able to tell you wind speed, what came through, etc.
cant run away from parent materials

6.What are buried Ahorizons?  Explain.
	Topsoil. Normally black in color once buried under other layers of soil. 

7.Which soil forming factor includes humans?
**** Biota ****

Different Soil Forming Factors:

Factor 1: Parent Material
Determines
Texture
Fertility
Types of clay minerals
Classes based on placement:

Factor 2: climate
Affects soil formation in 3 ways. 
Precipitation
Temperature
Native vegetation

Factor 3: Biota
Plants, animals, microorganisms
Important for many processes in soil formation
Chemical weathering (organic acids, carbonic acid, etc)
OM accumulation (water holding, nutrient holding)
Aggregation (polysaccharides)

Factor 4: Topography
Affeccts amount of water soil "sees"

Factor 5: Time
Works in concert with other  factors
Chronollogically old soil may be developmentally young, e.g., arid soils, little development

8.What would an old soil look like?
	very deep. can be productive. if you take care of them you can grow just about anything.

9.Understand the path of the Nile, the rivers that make it up, the countries that feed it
	

10.Could one call the Nile Delta the gift of Ethiopia?  Why?
	yes, because it carries the bedload. bedload includes volcanic ash which is very important.

11.Is there value in the sediment carried by the Nile (that is now filling the lakes)? Why was flooding important?
	Nile = worlds longest rivver. Drains 10% of Africa
	Blue Nile from Ethiopia (%80)
	White Nile from Lake Victoria (20%)
	Water and sediment. 
	Migration towards river during drier period(8000BC)
	flooding provided all the fertilizing and most of the irrigation
	Yes there is over centuries. annual flooding(1mm silt/year)


	
12.Be able to explain changes that occurred once agriculture provided the basis for the civilization
	Util 5,500 BC, hunter gatherers grinded wild grains and first settlement.
	found capital(memphis) at the border of Upper and Lower Egypt
	Writing(heiroglyphics)
	Irrigation agriculture becomes widespread; makes surpluses possible. 
	Civilizations die out when trade dies out.
	King Tut = Tombs
	New Kingdom Dynasty XVII
	Dynasty XXI to XXVI
	Dynasty XXVII
	Egypt soil has remained suitable for cropping for 6,000 years and more. 


13.Who were the Nubians?
	A group of Africans that lived in modern day Sudan and southern Egypt. They were early inhabitants of the Cradle of Civilizations. 
	Dam is over 500 miles long. 
	150k people displaced at constructed
	Covered up ruins of Nubia. 
	Thousands of people worked day and night on the dam. 

	Irrigation 
		To better manage the flooding
		Grow more than one crop a year
		Improve crop production
	Canal building
		Organized by regional authorities 
		30 cubic meter moved per person
		Annual dredging 
	Cultivation
		Plows pulled by cattle
		Hoes for weeding
		Animals
	

14.What are the advantages/disadvantages of the Aswan Dams?
	First dam 1899-1902. 
	⅓ of the US
	Equals 89 km^3/year. 
	Advantages
		Hydro-electric power 15% of Egyp
		Increased irrigation = more food production, up to 3 crops a year. 
		Desert = Agriculture
	Disadvatages
		90k people were displacd
		Increase in water borne disease
		Erosion down stream
		More reliance on chemical fertilizers
		More salinitiy
		Less sediment = more erosion
		Changes in fish catch in mediterranean
	Nile Delta Soils:
		Mostly very young soils(entisols)
		10m of river sediment over sand
		shrink swell clays
		very high population density
		less than 2m above sea water
	Modern Day
		Valley 18 km wide
		Delta 220 km wide
		Rainfall low
		8 million acres

15.What are agricultural terraces and what is there purpose?  How do rock pavements form?
	Steps used to farm on mountains or hills. Rock pavements form when saltwater wears away the center portion of rock. This occurs as sedimentary rock erodes.
	Stops erosion when the wind blows the soil awat and leaves rock pavements.
16.In many countries crop residues are quite valuable.  Explain.
	They have to feed their cattle.


Notes::

